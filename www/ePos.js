var exec = require('cordova/exec');

exports.findPrinters = function (success, error) {
    exec(success, error, "ePos", "findPrinters", []);
};

exports.createBuilder = function () {
    var builder = new Builder();
    builder.defaultTextFormat();
    return builder;
};

function Builder() {
    this.commands = [];
}

Builder.prototype.formatText = function (size, alignment, bold) {
    size = size || 1;
    alignment = alignment || 'LEFT';
    bold = bold || false;
    this.commands.push({type: 'TEXT_FORMATTING', size: size, alignment: alignment, bold: bold});
    return this;
};

Builder.prototype.defaultTextFormat = function () {
    return this.formatText(1);
};

Builder.prototype.addNewLine = function () {
    return this.addText("\n");
};

Builder.prototype.addText = function (text) {
    this.commands.push({type: 'TEXT', value: text});
    return this;
};

Builder.prototype.addBarcode = function (value) {
    this.commands.push({type: 'BARCODE', value: value});
    return this;
};

Builder.prototype.sendToPrinter = function (ip, success, error) {
    exec(success, error, "ePos", "doPrint", [ip, this.commands]);
};

Builder.prototype.toConsole = function () {
    console.log(this.commands);
};


