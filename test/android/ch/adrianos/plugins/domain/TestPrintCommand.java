package ch.adrianos.plugins.domain;

import ch.adrianos.plugins.domain.adapter.BuilderAdapter;
import ch.adrianos.plugins.domain.commands.AbstractPrintCommand;
import ch.adrianos.plugins.domain.commands.TextAlignment;
import ch.adrianos.plugins.domain.commands.TextFormatterPrintCommand;
import ch.adrianos.plugins.domain.commands.TextPrintCommand;
import com.epson.eposprint.EposException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class TestPrintCommand {

    public static void main(String[] args) throws EposException {

        List<AbstractPrintCommand> commandList = new ArrayList<AbstractPrintCommand>();
        commandList.add(new TextFormatterPrintCommand(2, TextAlignment.LEFT, false));
        commandList.add(new TextPrintCommand("Hello World"));
        final BuilderAdapter builderAdapter = dummyBuilderAdapter();
        commandList.forEach(new Consumer<AbstractPrintCommand>() {
            @Override
            public void accept(AbstractPrintCommand abstractPrintCommand) {
                try {
                    abstractPrintCommand.handle(builderAdapter);
                } catch (EposException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static BuilderAdapter dummyBuilderAdapter() {
        return new BuilderAdapter() {
            @Override
            public void addTextSize(int width, int height) throws EposException {

            }

            @Override
            public void addText(String data) throws EposException {

            }

            @Override
            public void addBarcode(String data, int type, int hri, int font, int width, int height) throws EposException {

            }

            @Override
            public void addTextAlign(int align) throws EposException {

            }

            @Override
            public void addTextStyle(int reverse, int ul, int em, int color) throws EposException {

            }
        };
    }

}
