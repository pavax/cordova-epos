package ch.adrianos.plugins;

import ch.adrianos.plugins.domain.adapter.BuilderAdapter;
import ch.adrianos.plugins.domain.adapter.internal.EpsonBuilderAdapter;
import ch.adrianos.plugins.domain.commands.AbstractPrintCommand;
import ch.adrianos.plugins.domain.commands.PrintCommandAssembler;
import com.epson.eposprint.Builder;
import com.epson.eposprint.EposException;
import com.epson.eposprint.Print;
import com.epson.epsonio.*;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ePos extends CordovaPlugin {

    private static final String TAG = "ePos-Cordvoa-Plugin";

    public static final int DEFAULT_SEND_DATA_TIMEOUT = 5000;

    public static final int COULD_NOT_PRINT_DATA = 20;

    public static final int SEVERE_EXCEPTION = 100;
    public static final String FIND_OPTION = "255.255.255.255";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("doPrint")) {
            String ip = args.getString(0);
            JSONArray jsonArrayOfCommands = args.getJSONArray(1);
            if (jsonArrayOfCommands == null) {
                callbackContext.error(createError("No Commands available", SEVERE_EXCEPTION));
                return true;
            }
            this.doPrinting(ip, callbackContext, PrintCommandAssembler.toPrintCommandList(jsonArrayOfCommands));
            return true;
        } else if (action.equals("findPrinters")) {
            this.findPrinters(callbackContext);
            return true;
        }
        return false;
    }

    private void findPrinters(CallbackContext callbackContext) {
        try {
            stopFinder();
            Finder.start(this.cordova.getActivity().getApplicationContext(), DevType.TCP, FIND_OPTION);
            cordova.getThreadPool().submit(new DiscoverPrinterRunnable(callbackContext));
        } catch (EpsonIoException e) {
            callbackContext.error(createError("", SEVERE_EXCEPTION));
        }
    }

    private void stopFinder() {
        //stop old finder
        while (true) {
            try {
                Finder.stop();
                break;
            } catch (EpsonIoException e) {
                if (e.getStatus() != IoStatus.ERR_PROCESSING) {
                    break;
                }
            }
        }
    }

    private void doPrinting(String ip, CallbackContext callbackContext, List<AbstractPrintCommand> printCommands) {
        try {
            Builder builder = new Builder("TM-T20II", Builder.MODEL_ANK);
            BuilderAdapter builderAdapter = new EpsonBuilderAdapter(builder);
            builder.addTextLang(Builder.LANG_EN);
            builder.addTextSmooth(Builder.TRUE);
            for (AbstractPrintCommand abstractPrintCommand : printCommands) {
                abstractPrintCommand.handle(builderAdapter);
            }
            doCut(builder);
            this.cordova.getThreadPool().submit(new SendPrintDataRunnable(ip, callbackContext, builder));
        } catch (Exception e) {
            callbackContext.error(createError("Could not prepare print data", SEVERE_EXCEPTION));
        }
    }

    private static void doCut(Builder builder) throws EposException {
        builder.addText("\n");
        builder.addText("\n");
        builder.addCut(Builder.CUT_FEED);
    }

    public static JSONObject createError(String message, int errorCode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("message", message);
            jsonObject.put("errorCode", errorCode);
        } catch (JSONException e) {
            throw new RuntimeException("Could not create JsonObject: ", e);
        }
        return jsonObject;
    }

    private static final class SendPrintDataRunnable implements Runnable {

        private final String ip;
        private final CallbackContext callbackContext;
        private final Builder builder;

        public SendPrintDataRunnable(String ip, CallbackContext callbackContext, Builder builder) {
            this.ip = ip;
            this.callbackContext = callbackContext;
            this.builder = builder;
        }

        @Override
        public void run() {
            final Print printer = new Print();
            try {
                printer.openPrinter(Print.DEVTYPE_TCP, ip, Print.PARAM_DEFAULT, Print.PARAM_DEFAULT);
                int[] status = new int[1];
                printer.sendData(builder, DEFAULT_SEND_DATA_TIMEOUT, status);
                callbackContext.success(status[0]);
            } catch (EposException e) {
                callbackContext.error(createError("Could print data", COULD_NOT_PRINT_DATA));
            } finally {
                try {
                    printer.closePrinter();
                } catch (EposException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static final class DiscoverPrinterRunnable implements Runnable {

        private final CallbackContext callbackContext;

        public DiscoverPrinterRunnable(CallbackContext callbackContext) {
            this.callbackContext = callbackContext;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(2000);
                DeviceInfo[] deviceInfoList = Finder.getDeviceInfoList(FilterOption.PARAM_DEFAULT);
                JSONArray jsonArray = new JSONArray();
                if (deviceInfoList == null || deviceInfoList.length == 0) {
                    callbackContext.success(jsonArray);
                    return;
                }
                for (DeviceInfo deviceInfo : deviceInfoList) {
                    JSONObject obj = new JSONObject();
                    obj.put("ipAddress", deviceInfo.getIpAddress());
                    obj.put("printerName", deviceInfo.getPrinterName());
                    jsonArray.put(obj);
                }
                callbackContext.success(jsonArray);
            } catch (EpsonIoException e) {
                callbackContext.error(createError("EpsonIoException", SEVERE_EXCEPTION));
            } catch (JSONException e) {
                throw new RuntimeException("Could not create JsonObject:", e);
            } catch (InterruptedException e) {
                throw new RuntimeException("Error occurred during sleep:", e);
            }
        }
    }
}
