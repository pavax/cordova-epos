package ch.adrianos.plugins.domain.commands;

public enum PrintCommandType {
    TEXT_FORMATTING, BARCODE, TEXT
}
