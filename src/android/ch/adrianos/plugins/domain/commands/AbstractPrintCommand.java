package ch.adrianos.plugins.domain.commands;

import ch.adrianos.plugins.domain.adapter.BuilderAdapter;
import com.epson.eposprint.EposException;

public abstract class AbstractPrintCommand {

    private final PrintCommandType printCommandType;

    public AbstractPrintCommand(PrintCommandType printCommandType) {
        this.printCommandType = printCommandType;
    }

    public abstract void handle(BuilderAdapter builderAdapter) throws EposException;

    public PrintCommandType getPrintCommandType() {
        return printCommandType;
    }
}
