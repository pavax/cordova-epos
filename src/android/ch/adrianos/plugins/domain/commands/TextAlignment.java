package ch.adrianos.plugins.domain.commands;

import com.epson.eposprint.Builder;

public enum TextAlignment {

    LEFT(Builder.ALIGN_LEFT), CENTER(Builder.ALIGN_CENTER), RIGHT(Builder.ALIGN_RIGHT);

    private final int eposApiCommand;

    TextAlignment(int eposApiCommand) {

        this.eposApiCommand = eposApiCommand;
    }

    public int getEposApiCommand() {
        return eposApiCommand;
    }
}
