package ch.adrianos.plugins.domain.commands;

import ch.adrianos.plugins.domain.adapter.BuilderAdapter;
import com.epson.eposprint.EposException;

public class TextPrintCommand extends AbstractPrintCommand {

    private final String value;

    public TextPrintCommand(String value) {
        super(PrintCommandType.TEXT);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public void handle(BuilderAdapter builderAdapter) throws EposException {
        builderAdapter.addText(this.value);
    }
}
