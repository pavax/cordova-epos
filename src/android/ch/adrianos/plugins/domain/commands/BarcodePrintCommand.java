package ch.adrianos.plugins.domain.commands;

import ch.adrianos.plugins.domain.adapter.BuilderAdapter;
import com.epson.eposprint.Builder;
import com.epson.eposprint.EposException;

public class BarcodePrintCommand extends AbstractPrintCommand {

    private final String value;

    public BarcodePrintCommand(String value) {
        super(PrintCommandType.BARCODE);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public void handle(BuilderAdapter builderAdapter) throws EposException {
        String data = value.replaceAll("-", "");
        builderAdapter.addBarcode(
                String.format("{B%s", data),
                Builder.BARCODE_CODE128,
                Builder.HRI_BELOW,
                Builder.PARAM_UNSPECIFIED,
                Builder.PARAM_UNSPECIFIED,
                50
        );
    }
}
