package ch.adrianos.plugins.domain.commands;

import ch.adrianos.plugins.domain.adapter.BuilderAdapter;
import com.epson.eposprint.Builder;
import com.epson.eposprint.EposException;

public class TextFormatterPrintCommand extends AbstractPrintCommand {

    private final int size;

    private final TextAlignment alignment;

    private final boolean bold;

    public TextFormatterPrintCommand(int size, TextAlignment alignment, boolean bold) {
        super(PrintCommandType.TEXT_FORMATTING);
        this.size = size;
        this.alignment = alignment;
        this.bold = bold;
    }

    public int getSize() {
        return size;
    }

    public TextAlignment getAlignment() {
        return alignment;
    }

    public boolean isBold() {
        return bold;
    }

    @Override
    public void handle(BuilderAdapter builderAdapter) throws EposException {
        builderAdapter.addTextStyle(Builder.PARAM_UNSPECIFIED, Builder.PARAM_UNSPECIFIED, isBold() ? Builder.TRUE : Builder.FALSE, Builder.PARAM_UNSPECIFIED);
        builderAdapter.addTextAlign(this.alignment.getEposApiCommand());
        builderAdapter.addTextSize(this.size, this.size);
    }
}
