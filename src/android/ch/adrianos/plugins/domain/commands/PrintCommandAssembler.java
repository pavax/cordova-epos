package ch.adrianos.plugins.domain.commands;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public final class PrintCommandAssembler {

    private PrintCommandAssembler() {
    }

    public static List<AbstractPrintCommand> toPrintCommandList(JSONArray jsonArray) throws JSONException {
        List<AbstractPrintCommand> printCommands = new LinkedList<AbstractPrintCommand>();
        for (int i = 0; i < jsonArray.length(); i++) {
            printCommands.add(toPrintCommand(jsonArray.getJSONObject(i)));
        }
        return printCommands;
    }

    private static AbstractPrintCommand toPrintCommand(JSONObject jsonObject) throws JSONException {
        PrintCommandType printCommandType = PrintCommandType.valueOf(jsonObject.getString("type"));
        switch (printCommandType) {
            case BARCODE:
                return toBarcodePrintCommand(jsonObject);
            case TEXT:
                return toTextPrintCommand(jsonObject);
            case TEXT_FORMATTING:
                return toTextFormattingCommand(jsonObject);
            default:
                throw new IllegalArgumentException("Unknown Type: " + printCommandType);
        }
    }

    private static BarcodePrintCommand toBarcodePrintCommand(JSONObject jsonObject) throws JSONException {
        return new BarcodePrintCommand(jsonObject.getString("value"));
    }

    private static TextPrintCommand toTextPrintCommand(JSONObject jsonObject) throws JSONException {
        return new TextPrintCommand(jsonObject.getString("value"));
    }

    private static TextFormatterPrintCommand toTextFormattingCommand(JSONObject jsonObject) throws JSONException {
        return new TextFormatterPrintCommand(jsonObject.getInt("size"), TextAlignment.valueOf(jsonObject.getString("alignment")), jsonObject.getBoolean("bold"));
    }
}
