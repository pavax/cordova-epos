package ch.adrianos.plugins.domain.adapter.internal;

import ch.adrianos.plugins.domain.adapter.BuilderAdapter;
import com.epson.eposprint.Builder;
import com.epson.eposprint.EposException;

public class EpsonBuilderAdapter implements BuilderAdapter {

    private final Builder builder;

    public EpsonBuilderAdapter(Builder builder) {
        this.builder = builder;
    }

    @Override
    public void addTextSize(int width, int height) throws EposException {
        builder.addTextSize(width, height);
    }

    @Override
    public void addText(String data) throws EposException {
        builder.addText(data);
    }

    @Override
    public void addBarcode(String data, int type, int hri, int font, int width, int height) throws EposException {
        builder.addBarcode(data, type, hri, font, width, height);
    }

    @Override
    public void addTextAlign(int align) throws EposException {
        builder.addTextAlign(align);
    }

    @Override
    public void addTextStyle(int reverse, int ul, int em, int color) throws EposException {
        builder.addTextStyle(reverse, ul, em, color);
    }
}
