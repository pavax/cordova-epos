package ch.adrianos.plugins.domain.adapter;

import com.epson.eposprint.EposException;

public interface BuilderAdapter {

    void addTextSize(int width, int height) throws EposException;

    void addText(String data) throws EposException;

    void addBarcode(String data, int type, int hri, int font, int width, int height) throws EposException;

    void addTextAlign(int align) throws EposException;

    void addTextStyle(int reverse, int ul, int em, int color) throws EposException;
}
